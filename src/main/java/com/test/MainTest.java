package com.test;

import com.test.util.MainRunner;
import org.testng.annotations.Test;
import com.test.pages.LoginPage;




public class MainTest extends MainRunner {
	
	
	@Test
	public void orangehrm_login_adduser_test() throws Exception {
		new LoginPage(driver).
		
		
			Check_And_Validate_Login_Page_UI_Elements().
			Step_Enter_Login_Details("Admin", "admin123").
			Step_Click_Login_Button().
			
			
			Check_And_Validate_Header_UI_Elements().
			Step_Navigate_To_Admin_Menu().
			Check_And_Validate_Admin_Page_UI_Elements().
			Step_Click_Add_Button().
			Check_And_Validate_AddUser_Page_UI_Elements().
			Step_Enter_Details("Admin", "Joe Root", "Joe"+Math.floor(Math.random() * 100000)+"Root", "Disabled", "reaper123@", "reaper123@").
			Check_And_Validate_Selections("Admin", "Disabled").
			Step_Click_Save_Button().
			Check_And_Validate_Admin_Page_UI_Elements().
			
			
			
			Step_Click_MyInfo_Button().
			Step_Click_Edit_Button().
			Step_Edit_Details("Ryan", "0987", "478523", "885", "7852", "Single", "Sri Lankan", "Jerry", "No", "1998-10-18", "2022-04-04").
			Step_Click_Save_Button().
		
			Step_Navigate_To_Image_Upload().
			Step_Upload_Image("C://success.png").
			Step_Click_Upload_Button();
			
			
			
		
	}
 
}