package com.test.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

import com.test.panels.Header;
import com.test.util.MainRunner;




public class AddUser extends MainRunner {
	WebDriver driver;
	Select userRole;
	WebElement empName;
	WebElement userName;
	Select status;
	WebElement pswrd;
	WebElement confpswrd;
	WebElement savebtn;
	WebElement cancelbtn;
	Header headerPanel;

	public AddUser (WebDriver driver) {
		super();
		this.driver = driver;	
		this.userRole = new Select(driver.findElement(By.id("systemUser_userType")));
		this.empName = driver.findElement(By.id("systemUser_employeeName_empName"));
		this.userName = driver.findElement(By.id("systemUser_userName"));
		this.status = new Select(driver.findElement(By.id("systemUser_status")));
		this.pswrd = driver.findElement(By.id("systemUser_password"));
		this.confpswrd = driver.findElement(By.id("systemUser_confirmPassword"));
		this.savebtn = driver.findElement(By.id("btnSave"));
		this.cancelbtn = driver.findElement(By.id("btnCancel"));
	}
	
	
	
	private AddUser action_Check_And_Validate_AddUser_Page_UI_Elements() throws Exception {
		assertEquals(userRole.getFirstSelectedOption().getText(), "ESS", "Add user page user role dropdown validation");
		assertTrue("Employee name field validation", empName.getText().isEmpty());
		assertTrue("Username field validation", userName.getText().isEmpty());
		assertEquals(status.getFirstSelectedOption().getText(), "Enabled", "Add user page status dropdown validation");
		assertTrue("Password field validation", pswrd.getText().isEmpty());
		assertTrue("Confirm password field validation", confpswrd.getText().isEmpty());
		assertTrue("Save button validation", savebtn.isEnabled());
		assertTrue("Cancel button validation", cancelbtn.isEnabled());
		return this;
	}
	
	public AddUser Check_And_Validate_AddUser_Page_UI_Elements() throws Exception {
		return action_Check_And_Validate_AddUser_Page_UI_Elements();
	}
	
	
	
	
	private AddUser action_Enter_Details(String role, String ename, String eUsername, String stts, String pswd, String cPswd) throws Exception {
				
		userRole.selectByVisibleText(role);
		empName.sendKeys(ename);
		userName.sendKeys(eUsername);
		status.selectByVisibleText(stts);
		pswrd.sendKeys(pswd);
		confpswrd.sendKeys(cPswd);
		return this;
	}
	
	public AddUser Step_Enter_Details(String role, String ename, String eUsername, String stts, String pswd, String cPswd) throws Exception {
		return action_Enter_Details(role, ename, eUsername, stts, pswd, cPswd);
	}
	
	
	
	
	private AddUser action_Check_And_Validate_Selections(String role, String stts) throws Exception {
		assertEquals(userRole.getFirstSelectedOption().getText(), role, "Entered user role dropdown validation");
		assertEquals(status.getFirstSelectedOption().getText(), stts, "Entered status dropdown validation");
		return this;
	}
	
	public AddUser Check_And_Validate_Selections(String role, String stts) throws Exception {
		return action_Check_And_Validate_Selections(role, stts);
	}
	
	

	
	private AdminPage action_Click_Save_Button() throws Exception {
		Thread.sleep(1000);
		savebtn.click();
		return new AdminPage(driver);
	}
	
	public AdminPage Step_Click_Save_Button() throws Exception {
		return action_Click_Save_Button();
	}
	
}




