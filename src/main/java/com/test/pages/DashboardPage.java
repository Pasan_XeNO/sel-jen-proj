package com.test.pages;

import org.openqa.selenium.WebDriver;

import com.test.panels.Header;
import com.test.util.MainRunner;

public class DashboardPage extends MainRunner {

	WebDriver driver;
	Header headerPanel;
	
	public DashboardPage(WebDriver driver) {
		super();
		this.driver = driver;	
		this.headerPanel = new Header(driver);
	}
	
	public DashboardPage Check_And_Validate_Header_UI_Elements() {
		this.headerPanel.action_Check_And_Validate_Header_UI_Elements();
		return this;
	}
	
	public AdminPage Step_Navigate_To_Admin_Menu() {
		this.headerPanel.action_Navigate_To_Admin_Menu();
		return new AdminPage(driver);
	}
	
	


	
}