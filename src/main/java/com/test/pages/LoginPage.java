package com.test.pages;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.test.util.MainRunner;

public class LoginPage extends MainRunner {
	WebDriver driver;

	WebElement txtUserName;
	WebElement txtPassWord;
	WebElement btnLogin;
	WebElement panelHeading;
	
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;
		this.txtUserName = driver.findElement(By.id("txtUsername"));
		this.txtPassWord = driver.findElement(By.id("txtPassword"));
		this.btnLogin = driver.findElement(By.id("btnLogin"));
		this.panelHeading = driver.findElement(By.id("logInPanelHeading"));
	}
	
	
	
	
	private LoginPage action_Validate_Login_Page_UI_Elements() throws Exception {
		assertEquals(panelHeading.getText(), "LOGIN Panel", "Panel heading validation");
		assertTrue("Username field validation", txtUserName.getText().isEmpty());
		assertTrue("Password field validation", txtPassWord.getText().isEmpty());
		assertTrue("Login button validation", btnLogin.isEnabled());
		return this;
	}
	
	public LoginPage Check_And_Validate_Login_Page_UI_Elements() throws Exception {
		return action_Validate_Login_Page_UI_Elements();
	}
	
	
	
		
	private LoginPage action_Enter_Login_Details(String username, String password) throws Exception {
		Thread.sleep(500);
		txtUserName.sendKeys(username);
		Thread.sleep(500);
		txtPassWord.sendKeys(password);
		return this;
	}
	
	public LoginPage Step_Enter_Login_Details(String username, String password) throws Exception {
		return action_Enter_Login_Details(username, password);
	}
	
	
	
	
	private DashboardPage action_Click_Login_Button() throws Exception {
		btnLogin.click();
		return new DashboardPage(driver);
	}
	
	public DashboardPage Step_Click_Login_Button() throws Exception {
		return action_Click_Login_Button();
	}
	

}
