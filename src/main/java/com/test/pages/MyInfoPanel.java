package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.test.util.MainRunner;

public class MyInfoPanel extends MainRunner {
	WebDriver driver;
	WebElement editAddbtn;
	WebElement middleName;
	WebElement otherId;
	WebElement driverLicense;
	WebElement ssnNumber;
	WebElement sinNumber;
	Select marital;
	Select nationality;
	WebElement nickName;
	WebElement milService;
	WebElement dob;
	WebElement lcncexp;

	public MyInfoPanel (WebDriver driver) {
		super();
		this.driver = driver;
		this.editAddbtn = driver.findElement(By.id("btnSave"));
		this.middleName = driver.findElement(By.id("personal_txtEmpMiddleName"));
		this.otherId = driver.findElement(By.id("personal_txtOtherID"));
		this.driverLicense = driver.findElement(By.id("personal_txtLicenNo"));
		this.ssnNumber = driver.findElement(By.id("personal_txtNICNo"));
		this.sinNumber = driver.findElement(By.id("personal_txtSINNo"));
		this.marital = new Select(driver.findElement(By.id("personal_cmbMarital")));
		this.nationality = new Select(driver.findElement(By.id("personal_cmbNation")));
		this.nickName = driver.findElement(By.id("personal_txtEmpNickName"));
		this.milService = driver.findElement(By.id("personal_txtMilitarySer"));
		this.dob = driver.findElement(By.id("personal_DOB"));
		this.lcncexp = driver.findElement(By.id("personal_txtLicExpDate"));
		
	}
	
	private MyInfoPanel action_Click_Edit_Button() throws Exception {
		editAddbtn.click();
		return this;
	}
	
	public MyInfoPanel Step_Click_Edit_Button() throws Exception {
		return action_Click_Edit_Button();
	}
	
	
	private MyInfoPanel action_Edit_Details(String midname, String idtwo, String dlicense, String ssn, String sin, String mart, String national, String nick, String milser, String birth, String licenseexp) throws InterruptedException {
		middleName.clear();
		middleName.sendKeys(midname);
		otherId.clear();
		otherId.sendKeys(idtwo);
		driverLicense.clear();
		driverLicense.sendKeys(dlicense);
		ssnNumber.clear();
		ssnNumber.sendKeys(ssn);
		sinNumber.clear();
		sinNumber.sendKeys(sin);
		marital.selectByVisibleText(mart);
		nationality.selectByVisibleText(national);
		nickName.clear();
		nickName.sendKeys(nick);
		milService.clear();
		milService.sendKeys(milser);
		dob.clear();
		dob.sendKeys(birth);
		dob.sendKeys(Keys.RETURN);
		lcncexp.clear();
		lcncexp.sendKeys(licenseexp);
		lcncexp.sendKeys(Keys.RETURN);
		return this;
		
	}
	
	public MyInfoPanel Step_Edit_Details(String midname, String idtwo, String dlicense, String ssn, String sin, String mart, String national, String nick, String milser, String birth, String licenseexp) throws Exception{
		return action_Edit_Details(midname, idtwo, dlicense, ssn, sin, mart, national, nick, milser, birth, licenseexp);
	}
	
	private MyInfoPanel action_Click_Save_Button() throws Exception {
		Thread.sleep(1000);
		editAddbtn.click();
		return this;
	}
	
	public MyInfoPanel Step_Click_Save_Button() throws Exception {
		return action_Click_Save_Button();
	}
	
	private UploadImagePage action_Navigate_To_Image_Upload() throws Exception {
		Thread.sleep(4000);
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/pim/viewPhotograph/empNumber/7");
		return new UploadImagePage(driver);
	}
	
	public UploadImagePage Step_Navigate_To_Image_Upload() throws Exception {
		return action_Navigate_To_Image_Upload();
	}
	
	

}
 