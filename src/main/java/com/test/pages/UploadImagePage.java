package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.test.util.MainRunner;

public class UploadImagePage extends MainRunner {
	WebElement choosefile;
	WebElement btnUpload;
	
	
	public UploadImagePage (WebDriver driver) {
		super();
		this.choosefile = driver.findElement(By.id("photofile"));
		this.btnUpload = driver.findElement(By.id("btnSave"));
		
	}
	
	private UploadImagePage action_Upload_Image(String imgpath) throws Exception{
		choosefile.sendKeys(imgpath);
		return this;
	}
	
	public UploadImagePage Step_Upload_Image(String imgpath) throws Exception{
		return action_Upload_Image(imgpath);
	}
	
	private UploadImagePage action_Click_Upload_Button() throws Exception{
		btnUpload.click();
		return this;
	}
	
	public UploadImagePage Step_Click_Upload_Button() throws Exception{
		return action_Click_Upload_Button();
	}
}
