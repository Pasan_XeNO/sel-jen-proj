package com.test.util;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeSuite;


import io.github.bonigarcia.wdm.WebDriverManager;

public class MainRunner {
	protected WebDriver driver;

	@BeforeSuite
	public void connection_initiate() throws Exception {
		preDriverInit();
		WebDriverManager.chromedriver().setup();
		this.driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		driver.manage().window().maximize();
		postDriverInit();
	}
	
    protected void preDriverInit() {
    }

    protected void postDriverInit() throws Exception {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

	
	@AfterSuite
	public void connection_terminate() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}

}
